package Lab9;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;


public class Main {
	static StopWatch sw = new StopWatch();
	public static void main(String [] args) throws IOException{
		WordCounter counter = new WordCounter();
		String FILE_URL = "http://se.cpe.ku.ac.th/dictionary";
		URL url = new URL( FILE_URL);
		sw.start();
		System.out.printf("Total Words : %d\n",counter.countWords(url.openStream()));
		sw.stop();
		System.out.printf("Total Syllables : %d\n",counter.totalSyllables);
		System.out.printf("Elapsed Time : %f",sw.getElapsed());
	}
}
