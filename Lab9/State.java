package Lab9;
/**
 * State interface for counting syllables.
 * @author Natanon Poonawagul
 *
 */
public interface State {
	/**
	 * The method for checking character for counting syllables.
	 * @param c is the character for checking.
	 */
	void handleChar(char c);
	/**
	 * The method doing things when enter the state.
	 */
	void enterState();
}
