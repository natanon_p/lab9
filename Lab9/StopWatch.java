package Lab9;
/**
 * The StopWatch using for get time elapsed.
 * @author Natanon Poonawagul
 *
 */
public class StopWatch {
	/** A constant Nanoseconds time. */
	private static final double NANOSECONDS = 1.0E-9;
	/** A starting time. */
	private long startTime;
	/** A boolean check whether this StopWatch is running or not. */
	private boolean isStart = false;
	/**
	 * Method running the time of StopWatch object.
	 */
	public void start(){
		if(isRunning()){
			
		}
		else{
			isStart = true;
			startTime = System.nanoTime();
		}
	}
	/**
	 * Method stop the time of StopWatch object.
	 */
	public void stop(){
		if(isRunning()){
			isStart = false;
		}
		else{
			
		}
	}
	/**
	 * Method check whether the StopWatch is running or not.
	 * @return true if StopWatch is running otherwise false.
	 */
	public boolean isRunning(){
		if(isStart)
			return true;
		return false;
	}
	/**
	 * Method get elapsed time that using in work.
	 * @return elapsed time.
	 */
	public double getElapsed(){
		double elapsedTime = System.nanoTime()-startTime;
		return elapsedTime / 1000000000.0;
	}
}
