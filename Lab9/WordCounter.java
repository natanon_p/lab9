package Lab9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
/**
 * WordCounter class for counting the words and syllables.
 * @author Natanon Poonawagul
 *
 */
public class WordCounter {
	/** state show you what the current state is.*/
	private State state;
	/** syllables is the amount of syllables of the whole word.*/
	private int syllables;
	/** totalSyllables is the total syllables of the whole sentenses.*/
	public int totalSyllables = 0;
	/**
	 * A start state which entered when received the first character.
	 */
	private final State START = new State(){
		/**
		 * This state method checking character for going to next state. 
		 * @param c is the character to be checked in this method.
		 */
		public void handleChar(char c){
			if(c == 'e' || c == 'E')
				setState(E_FIRST);
			else if (isVowel(c) || c == 'y' || c == 'Y')
				setState(VOWEL);
			else if(Character.isLetter(c))
				setState(CONSONANT);
			else
				setState(NONWORD);
		}
		/**
		 * This state method doing nothing when entered the state.
		 */
		public void enterState(){
			
		}
	};
	/**
	 * A consonant state entered when the received character is consonant.
	 */
	private final State CONSONANT = new State(){
		/**
		 * This state method checking the character for going to next state.
		 * @param c is the character to be checked.
		 */
		public void handleChar(char c){
			if(c == 'e' || c == 'E')
				setState(E_FIRST);
			else if(isVowel(c) || c == 'y' || c == 'Y')
				setState(VOWEL);
			else if(Character.isLetter(c))
				setState(CONSONANT);
			else if(c == '-')
				setState(DASH);
			else
				setState(NONWORD);
		}
		/**
		 * This state method doing nothing when entered the state.
		 */
		public void enterState(){
			
		}
	};
	/**
	 * A vowel state entered when the received character is vowel.
	 */
	private final State VOWEL = new State(){
		/**
		 * A method checking the character for entering other state.
		 * @param c is character to be checked.
		 */
		public void handleChar(char c){
			if(isVowel(c))
				setState(VOWEL);
			else if(Character.isLetter(c))
				setState(CONSONANT);
			else if(c == '-')
				setState(DASH);
			else
				setState(NONWORD);
		}
		/**
		 * This state count 1 syllables when entered.
		 */
		public void enterState(){
			syllables++;
		}
	};
	/**
	 * A E state entered when the received character is the first e after consonant.
	 */
	private final State E_FIRST = new State(){
		/**
		 * A method checking character for going to next state.
		 * @param c is the character to be checked.
		 */
		public void handleChar(char c){
			if(isVowel(c))
				setState(VOWEL);
			else if(Character.isLetter(c)){
				syllables++;
				setState(CONSONANT);
			}
			else if(c == '-')
				setState(DASH);
			else
				setState(NONWORD);
		}
		/**
		 * This method doing nothing when entered the state.
		 */
		public void enterState(){

		}
	};
	/**
	 * A state of non word occur when the received character is not a letter. 
	 */
	private final State NONWORD = new State(){
		/**
		 * This method doesn't do anything in this state.
		 */
		public void handleChar(char c){
			
		}
		/**
		 * When ever entered this state syllables is return to 0 (because it not a word).
		 */
		public void enterState(){
			syllables = 0;
		}
	};
	/**
	 * A state entered when the character is "-" (dash). 
	 */
	private final State DASH = new State(){
		/**
		 * A method checking the character for going to next state.
		 * @param c is the character to be checked in this method.
		 */
		public void handleChar(char c){
			if(isVowel(c))
				setState(VOWEL);
			else if(Character.isLetter(c))
				setState(CONSONANT);
			else
				setState(NONWORD);
		}
		/**
		 * This method doing nothing when entered this state.
		 */
		public void enterState(){
			
		}
	};
	/**
	 * A method using for counting words from inputStream.
	 * @param instream the inputStream to be counted words in this method.
	 * @return the number of words from inputStream.
	 */
	public int countWords(InputStream instream){
		syllables = 0;
		int count = 0;
		InputStreamReader streamReader = new InputStreamReader(instream);
		BufferedReader BReader = new BufferedReader(streamReader);
		while(true){
			try{
			String line = BReader.readLine(); 
			if (line == null) break;
			count++;
			totalSyllables += countSyllables(line);
			}catch(IOException e){
				e.printStackTrace();
			}
		}
		return count;
	}
	/**
	 * A method using to count syllables from the word.
	 * @param word is the String of word to be counted syllables in this method.
	 * @return the number of syllables in the word.
	 */
	public int countSyllables(String word){
		syllables = 0;
		state = START;
		char[] charArr = word.toCharArray();
		for(char c : charArr)
			state.handleChar(c);
		if(state == DASH)
			syllables = 0;
		if(state == E_FIRST && syllables == 0)
			syllables++;
		return syllables;
	}
	/**
	 * A method check whether the character is the vowel or not.
	 * @param c is the character to be checked as a vowel.
	 * @return true if char c is the vowel false otherwise.
	 */
	public boolean isVowel(char c){
		if(c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u')
			return true;
		if(c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U')
			return true;
		return false;
	}
	/**
	 * A set method changing the current state to the new state. 
	 * @param newState is the state to be changed into.
	 */
	public void setState(State newState){
		if(state != newState){
			state = newState;
			state.enterState();
		}
	}
}
